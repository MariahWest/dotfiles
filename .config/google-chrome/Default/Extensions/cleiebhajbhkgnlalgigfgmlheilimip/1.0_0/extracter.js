;(function(){

    const PREFIX = "__gvl__";
    const RESULT_CONTAINER_ID = PREFIX + "container";

    cleanUp();

    let videoLinks = [];

    let videoElements = document.querySelectorAll('video') || [];
    videoElements.forEach(el => {
        if (el.src && el.src.length > 0)
            videoLinks.push(el.src);
        let sources = el.querySelectorAll('source') || [];
        sources.forEach(source => {
            if (source.src)
                videoLinks.push(source.src);
        });
    });

    let wnd = create("div");
    wnd.id = RESULT_CONTAINER_ID;

    let backdrop = create("div", "backdrop");
    backdrop.addEventListener("click", event => {
        cleanUp();
    })
    wnd.appendChild(backdrop);

    let list = create("ul", "links");
    wnd.appendChild(list);

    if (videoLinks.length > 0) {
        videoLinks.forEach(link => {
            let listItem = create("a", "link");
            listItem.innerText = link;
            listItem.href = link;
            listItem.target = "_blank";
            list.appendChild( create("li").appendChild(listItem) );
        });
    } else {
        let msg = create("span", "msg");
        msg.innerText = "No videos found";
        list.appendChild( create("li").appendChild( msg ) );
    }


    document.body.appendChild(wnd);


    function create(tagName = "div", className = "") {
        let result = document.createElement(tagName);
        result.className = className.length > 0 ? PREFIX + className : "";
        return result;
    }

    function cleanUp() {
        let existingWindows = document.querySelectorAll(`#${RESULT_CONTAINER_ID}`) || [];
        existingWindows.forEach(wnd => {
            document.body.removeChild(wnd);
        });
    }

})();