syntax on
filetype plugin indent on

" General settings
set mouse=a
set shiftwidth=4 tabstop=4 softtabstop=4
set matchpairs+=<:>
set scrolloff=3
set backspace=indent,eol,start
set number
set splitright splitbelow
set ignorecase smartcase
nnoremap <silent> <Space>   :nohlsearch<CR>
set spelllang=en_us

" Filetype settings
augroup filetypes
    autocmd!
    " Git commits
    autocmd FileType gitcommit setl spell
    " Markdown
    autocmd FileType markdown setl spell tw=80 wrap
augroup END

map! § <ESC>
