#!/bin/bash
sudo docker run -d   -p 10.0.0.38:13378:80   -v /home/mariah/.config/audiobookself:/config   -v /home/mariah/audiobookself/metadata:/metadata   -v /mnt/Jellyfin/Audiobooks:/audiobooks  -v /mnt/Jellyfin/Books:/books  --name audiobookshelf   --rm ghcr.io/advplyr/audiobookshelf
exit
